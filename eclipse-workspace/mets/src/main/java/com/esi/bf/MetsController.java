package com.esi.bf;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin("*")
@Controller
@RequestMapping("/api/mets")
public class MetsController {
	
	@Autowired
	MetsService metsService;
	
	@PostMapping("/add")
	public ResponseEntity<?> create(@RequestBody Mets mets) {
		try {
			Mets saved = metsService.save(mets);
			return ResponseEntity.ok(saved);
		} catch (Exception e) {
			return ResponseEntity
					.badRequest()
					.body("\"Impossible de créer !\"");
		}
	}
	
	
	@GetMapping("/all")
	public ResponseEntity<?> getAll() {
		List<Mets> mets = metsService.findAll();
		return new ResponseEntity<List <Mets> >(mets, HttpStatus.OK);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> get(
			@PathVariable("id") Long id) {
		try {
			Optional<Mets> opt = metsService.getMet(id);
			return ResponseEntity.ok(opt.get());
		} catch (Exception e) {
			return ResponseEntity
					.badRequest()
					.body("\"Introuvable !\"");
		}
	}

	
	
	@PostMapping("/update/{id}")
	public ResponseEntity<?> update(
			@PathVariable("id") Long id,
			@RequestBody Mets mets) {
		try {
			Optional<Mets> opt = metsService.getMet(id);
			Mets current = opt.get();
			current.setDescription(mets.getDescription());
			current.setImage(mets.getImage());
			current.setNom(mets.getNom());
			current.setOrigine(mets.getOrigine());
			Mets update = metsService.save(current);
			return ResponseEntity.ok(update);
		} catch (Exception e) {
			return ResponseEntity
					.badRequest()
					.body("\"Impossible de modifier !\"");
		}
	}
	
	
	@PostMapping("/delete/{id}")
	public ResponseEntity<?> delete(
			@PathVariable("id") Long id) {
		try {
			metsService.delete(id);
			return ResponseEntity.ok("\"OK !\"");
		} catch (Exception e) {
			return ResponseEntity
					.badRequest()
					.body("\"Impossible de supprimer !\"");
		}
	}

}
