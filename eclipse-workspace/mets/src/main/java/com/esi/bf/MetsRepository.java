package com.esi.bf;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MetsRepository extends JpaRepository<Mets, Long> {

}
