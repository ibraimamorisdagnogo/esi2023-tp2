package com.esi.bf;

import java.util.List;
import java.util.Optional;

public interface MetsService {
	public Mets save(Mets mets);
	public List<Mets> findAll();
	public void delete(Long id);
	public Optional getMet(Long id);
}
