package com.esi.bf;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class MetsServiceImpl implements MetsService {

	@Autowired
	MetsRepository metsRepository;
	
	@Override
	public Mets save(Mets mets) {
		return metsRepository.save(mets);
	}

	@Override
	public List<Mets> findAll() {
		return metsRepository.findAll(Sort.by(Direction.ASC, "nom"));
	}

	@Override
	public void delete(Long id) {
		metsRepository.deleteById(id);
	}


	@Override
	public Optional<Mets> getMet(Long id) {
		return metsRepository.findById(id);
	}

}
